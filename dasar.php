<!DOCTYPE html>
<html>
<head>
    <title>Konversi Nilai</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>Konversi Nilai</h1>
    <form method="post" action="">
        Masukkan Nilai Angka: <input type="number" name="nilai" required>
        <input type="submit" name="submit" value="Konversi">
    </form>

    <?php
    if (isset($_POST['submit'])) {
        $nilai = $_POST['nilai'];
        if ($nilai >= 60) {
            echo "Nilai Anda $nilai, Anda Lulus";
        } else {
            echo "Nilai anda $nilai, Anda Gagal";
        }
    }
    ?>

    <h2>Konversi Segitiga</h2>
    <form method="post" action="">
        Masukkan Tinggi Segitiga: <input type="number" name="tinggi" required>
        <input type="submit" name="Submit" value="Konversi">
    </form>
    <?php
    if (isset($_POST['Submit'])) {
        $tinggi = $_POST['tinggi'];
        echo "<pre>";
        for ($baris = 1; $baris <= $tinggi; $baris++) {
            for ($i = 1; $i <= $tinggi - $baris; $i++) {
                echo " ";
            }
            for ($j = 1; $j < 2 * $baris; $j++) {
                echo "*";
            }
            echo "\n";
        }
        
    }
    ?>
</body>
</html>